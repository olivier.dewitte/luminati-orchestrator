#!/usr/bin/env python3

import requests
import json
import time
import docker

from multiprocessing import Pool

NB_ZONE = 4
MAX_THREAD = 5

def _create_proxies():  # USELESS
    """
    Create NB_ZONE zones and return them.
    :return: None
    """
    res = {}
    for index in range(NB_ZONE):
        data = requests.post(
            'http://luminati:22999/api/proxies', 
            data=json.dumps({
                'proxy': {'port': 24020+index},
                'zone': 'zone1'
            }), 
            headers={'content-type': 'application/json'}
        )
    time.sleep(10)
    return True

def delete_proxies(proxies_list):  # TODO useless
    """
    Delete Proxies
    :return: boolean
    """
    for proxy in proxies_list:
        requests.delete(
            f'http://luminati:22999/api/proxies/{proxy}',
            headers={'content-type': 'application/json'}
        )
    return True

def get_proxies_list(cpt=0):
    """
    Get and return running proxies
    :return: proxies list
    """
    data = requests.get('http://luminati:22999/api/proxies_running')
    
    if data.ok:
        data = data.json()
        return [f'luminati:{proxies_data["port"]}' for proxies_data in data]
    return []

def get_data(proxy, cpt=0):
    """
    Get DATA
    :return: dict
    """
    container_data = { 
        'image': 'luminati_scraper', 
        'network': 'luminati-orchestrator_internal_network',
        'auto_remove': True,
        'detach': True,
        'stdout': True,
        'environment': {
            'http_proxy': f'http://{proxy}',
            'https_proxy': f'https://{proxy}',
            'no_proxy': 'receiver,luminati,127.0.0.1'
        },
        'command': 'python /data/scraper.py'
    }

    try:
        client.containers.run(**container_data)
    except docker.errors.ContainerError as cont_error:
        print(f"Fail container {cont_error}")
    return True

def main():
    with Pool(MAX_THREAD) as pool:
        try:
            pool.map(get_data, get_proxies_list())
        except Exception as e:
            print(f"Fail: {e}")


if __name__ == '__main__':
    client = docker.from_env()
    main()
