const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
      headless: true, 
      args: ['--no-sandbox'],
      defaultViewport: {width: 1080, height: 1920}
    });
  const page = await browser.newPage();
  await page.goto('https://mon-ip.io/');
  await page.screenshot({path: '/data/example.png'});

  await browser.close();
})();