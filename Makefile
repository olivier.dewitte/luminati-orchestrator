build: build_orchestrator build_receiver build_scraper

build_orchestrator:
	docker-compose build --force-rm orchestrator

build_receiver:
	docker-compose build --force-rm receiver

build_scraper:
	docker-compose build --force-rm scraper
