from flask import Flask, request, Response, render_template, send_file, jsonify
from uuid import uuid4
from io import BytesIO
from PIL import Image
import base64
import json


DATA = {}
IMG_DATA = {}

app = Flask(__name__)

@app.route('/', methods=['GET'])
def show_data():
    return render_template('index.html', scraper=DATA
    )

@app.route('/api/image/<scraper_id>', methods=['GET'])
def api_get_image(scraper_id):
    """
    Search and return image based on id
    """
    if scraper_id not in DATA:
        return "404", 'File not found'

    image_data = DATA[scraper_id]['screenshot']
    ta = BytesIO()
    ta.write(image_data)
    ta.seek(0)

    return send_file(ta, mimetype="image/jpeg", as_attachment=False, attachment_filename='img.jpeg')

@app.route('/api/image/<scraper_id>', methods=['POST'])
def api_post_image(scraper_id):
    """
    Receive and save b64 encoded image
    """
    b64_data = base64.b64decode(request.data)

    if scraper_id not in DATA:
        DATA.update({
            scraper_id: {}
        })

    DATA[scraper_id]['screenshot'] = b64_data
    return jsonify({'res': 'OK'})


@app.route('/api/data/<scraper_id>', methods=['POST'])
def api_post(scraper_id):
    """
    Handle data from scraper
    """
    if scraper_id not in DATA:
        DATA.update({
            scraper_id: {}
        })

    data = json.loads(request.data)
    DATA[scraper_id].update(data)
    
    return jsonify({'res': 'OK'})


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)