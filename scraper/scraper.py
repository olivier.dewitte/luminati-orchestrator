import sys
import os
import requests
import json
import asyncio
from pyppeteer import launch
from tempfile import mkstemp
from base64 import b64encode
from uuid import uuid4


def main_test(receiver_url):
    data = requests.get('http://lumtest.com/myip.json')
    if data.ok:
        data = data.json()
    

    requests.post(
        f'http://{receiver_url}/api/',
        headers={'content-type': 'application/json'},
        data=json.dumps(data)
    )
    return True


async def main():
    """ Use pyppeteer to scrap data """
    browser = await launch(
        headless=True,
        options={'args': ['--no-sandbox']}
    )
    page = await browser.newPage()
    await page.goto('https://www.myip.com')

    tmp_file_path = mkstemp()[1]
    await page.screenshot({
        'path': tmp_file_path,
        'type': 'jpeg',
        'fullPage': True,
        })  # TODO: send image to 'receiver'.

    with open(tmp_file_path, 'rb') as tmp_file:
        b64_image = b64encode(tmp_file.read())

    # TODO: get ip information (country, city, isp) and return it to receiver.

    provider_data = await page.evaluate('''() => {
        return {
            'ip': document.getElementById('ip').innerText,
            'isp': document.getElementById('ajax').innerText,
            'country': document.getElementsByClassName('info')[0].getElementsByClassName('info_2')[0].innerText
        }
    }''')
    """
    ip = span id="ip"
    isp = div id="ajax"
    """
    scaper_id = uuid4().int
    requests.post(
        f"http://{receiver_url}/api/image/{scaper_id}",
        data=b64_image
    )

    requests.post(
        f"http://{receiver_url}/api/data/{scaper_id}",
        data=json.dumps(provider_data),
        headers={'content-type': 'application/json'},
    )

    await browser.close()

    """
    # TODO: Useless code
    dimensions = await page.evaluate('''() => {
        return {
            width: document.documentElement.clientWidth,
            height: document.documentElement.clientHeight,
            deviceScaleFactor: window.devicePixelRatio,
        }
    }''')

    
    requests.post(
        f'http://{receiver_url}/api/',
        headers={'content-type': 'application/json'},
        data=json.dumps(dimensions)
    )
    """
    # >>> {'width': 800, 'height': 600, 'deviceScaleFactor': 1}
    


if __name__ == "__main__":
    receiver_url = os.environ.get('RECEIVER_URL', 'receiver:5000')
    asyncio.get_event_loop().run_until_complete(main())